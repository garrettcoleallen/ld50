extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Panel/MuteCheckbox.pressed =AudioServer.is_bus_mute(0)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_accept") && !$NewGameBox/SeedInput.has_focus():
		_on_NewGameBtn_pressed()
	


func _on_NewGameBtn_pressed():
	Seed.world_seed = $NewGameBox/SeedInput.text
	get_tree().change_scene("res://Game.tscn")
	


func _on_LeaderboardBtn_pressed():
	get_tree().change_scene("res://leaderboard/Leaderboard.tscn")


func _on_TutorialBtn_pressed():
	$"Lastbreath-tutorial".visible = true



func _on_MuteCheckbox_toggled(button_pressed):
	AudioServer.set_bus_mute(0, button_pressed)


func _on_CloseTutorialBtn_pressed():
	$"Lastbreath-tutorial".visible = false
