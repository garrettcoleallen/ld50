class_name WaterSpawn
extends Node2D


export (NodePath) var WaterSystemPath

var water_system

export (int, 1, 8) var water_spawn_amount := 8

export (float) var water_spawn_frequency := 0.01

export (float) var water_spawn_lifetime := 5

# Called when the node enters the scene tree for the first time.
func _ready():
	if not water_system:
		water_system = get_node(WaterSystemPath)
		
	if !AudioServer.is_bus_mute(0):
		$AudioStreamPlayer2D.play()
		
	$Timer.wait_time = water_spawn_frequency
	$Timer.start(water_spawn_frequency)
	
	$LifetimeTimer.wait_time = water_spawn_lifetime
	$LifetimeTimer.start(water_spawn_lifetime)


func _on_Timer_timeout():
	water_system.spawn_water_global_pos(global_position, water_spawn_amount)


func _on_LifetimeTimer_timeout():
	$Timer.stop()
	queue_free()
