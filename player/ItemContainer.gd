extends Node2D


var active_item := 0

# Called when the node enters the scene tree for the first time.
func _ready():
	_update_active_item()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var item = _get_active_item()
	if item.has_method("get_cooldown_percent"):
		$CanvasLayer/ActiveItemBox/ItemCooldown.visible = true
		$CanvasLayer/ActiveItemBox/ItemCooldown.value = item.get_cooldown_percent() * 100
			
	$CanvasLayer/ActiveItemBox/ItemCooldown.visible = item.has_method("get_cooldown_percent") && $CanvasLayer/ActiveItemBox/ItemCooldown.value > 0.0
	$CanvasLayer/ActiveItemBox/UseItemHint.visible = !$CanvasLayer/ActiveItemBox/ItemCooldown.visible

func _physics_process(delta):
	if get_parent().is_action_just_pressed_alive("next_item"):
		active_item += 1
		if active_item >= $ActiveItem.get_child_count():
			active_item = 0
		_update_active_item()
		
	if get_parent().is_action_just_pressed_alive("use_item"):
		print("Trying use on ind", active_item)
		var item = _get_active_item()
		if item.has_method("use"):
			item.use()
			
func _update_active_item():
	var idx = 0
	for child in $ActiveItem.get_children():
		child.visible = idx == active_item
		idx += 1
	for child in $CanvasLayer/ActiveItemBox/ActiveItemSprite.get_children():
		child.get_parent().remove_child(child)
	var item = _get_active_item()
	if item.has_method("get_item_sprite"):
		$CanvasLayer/ActiveItemBox/ActiveItemSprite.add_child(item.get_item_sprite())
	
func _get_active_item() -> Node:
	return $ActiveItem.get_child(active_item)
