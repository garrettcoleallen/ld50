extends KinematicBody2D


const GRAVITY = 20
const ACCEL = 20
const RESPONSE_ACCEL = 60 # extra accel for opposite movement
const MAX_SPEED_X = 250
const FRICTION = 40
const JUMP_VELOCITY = 12
const INITIAL_JUMP_VELOCITY = 300
const MAX_HEALTH = 100

signal died(score)

export (NodePath) var tilemap_path
var tilemap : TileMap

var velocity = Vector2()
var move_vector = Vector2()
var holding_jump = false

var health = 100
var score := 0

var is_dead = false
var is_in_water := false # is player in water, changes movement
var is_underwater := false # is head underwater? Can they breathe basically
var is_climbing := false # is player climbing something?

# Called when the node enters the scene tree for the first time.
func _ready():
	tilemap = get_node(tilemap_path)
	update_health_bar()
	add_score(0)

#Check if action is pressed,
#only valid if hero is alive
func is_action_pressed_alive(name):
	if is_dead:
		return false
	return Input.is_action_pressed(name)
	
func is_action_just_pressed_alive(name):
	if is_dead:
		return false
	return Input.is_action_just_pressed(name)
	
func _process(delta):
	handle_graphics(delta)
	
func handle_graphics(delta):
	$PlayerUI/AirBar.value = $AirTimer.time_left / $AirTimer.wait_time * 100

func _physics_process(delta):
	if velocity.x < 0:
		$ItemContainer.scale.x = -1
	elif velocity.x > 0:
		$ItemContainer.scale.x = 1
		
	if !is_dead:
		check_for_water_area()
		check_for_underwater()
	
	
	if is_action_pressed_alive("move_left"):
		if velocity.x > 0:
			velocity.x -= RESPONSE_ACCEL
		velocity.x -= ACCEL
		move_vector.x = -1
	elif is_action_pressed_alive("move_right"):
		if velocity.x < 0:
			velocity.x += RESPONSE_ACCEL
		velocity.x += ACCEL
		move_vector.x = 1
	elif (is_on_floor() || is_climbing) and abs(velocity.x) > 0:
		# no move input so calculate friction decel
		# but only if on the floor
		var decel = velocity.x / abs(velocity.x) * -1 * FRICTION
		if abs(velocity.x) < abs(decel):
			velocity.x = 0
		else:
			velocity.x += decel
		move_vector.x = 0
		
	# if you are against a wall then you have zero velocity
	if is_on_wall():
		if is_action_pressed_alive("move_left"):
			mine_in_direction(Vector2.LEFT)
		elif is_action_pressed_alive("move_right"):
			mine_in_direction(Vector2.RIGHT)
		velocity.x = 0
		
		
	# If on the floor allow jump
	if is_climbing:
		$FallTimer.stop()
		take_fall_damage = false # fall damage negates if on a ladder
		if is_action_pressed_alive("move_down"):
			velocity.y += ACCEL
			if is_on_floor():
				mine_in_direction(Vector2.DOWN)
		elif is_action_pressed_alive("jump"):
			velocity.y -= ACCEL
		elif abs(velocity.y) > 0:
			var decel = velocity.y / abs(velocity.y) * -1 * FRICTION
			if abs(velocity.y) < abs(decel):
				velocity.y = 0
			else:
				velocity.y += decel
		
	elif is_on_floor():
		$FallTimer.stop()
		if take_fall_damage:
			if !is_in_water: #dont take damage if in water tho but cancel the fall
				take_damage(20)
			take_fall_damage = false
		
		if is_action_pressed_alive("move_down"):
			mine_in_direction(Vector2.DOWN)
			
		# on the floor, so stop jump allow timer
		# and reset velocity to 0 (on the floor!)
		$JumpAccelTimer.stop()
		velocity.y = 0
		
		if is_action_pressed_alive("jump"):
			var jump_velocity = INITIAL_JUMP_VELOCITY
			if is_in_water:
				jump_velocity = INITIAL_JUMP_VELOCITY/2.0
			velocity.y -= jump_velocity
			$JumpAccelTimer.start()
			holding_jump = true
			
	elif is_on_ceiling() && velocity.y < 0:
		#else if on the ceiling then cancel jump and start falling
		$JumpAccelTimer.stop()
		velocity.y = 0
		holding_jump = false
		
	elif (!$JumpAccelTimer.is_stopped() && holding_jump):
		#if holding jump keep speed up longer (for higher jump)
		if is_action_pressed_alive("jump"):
			velocity.y -= JUMP_VELOCITY
		else:
			holding_jump = false
	elif is_in_water && is_action_pressed_alive("jump"):
		velocity.y -= JUMP_VELOCITY/2
	else:
		if is_in_water && is_dead:
			velocity.y += GRAVITY/16 # slower float down when you die underwater DRAMA
		elif is_in_water:
			velocity.y += GRAVITY/4
		else:
			velocity.y += GRAVITY
			if velocity.y > 0 && $FallTimer.is_stopped():
				$FallTimer.start()
		
	
	var max_speed_x = MAX_SPEED_X
	if is_in_water:
		max_speed_x = MAX_SPEED_X/2.0
		
	# Check for max speed
	if abs(velocity.x) > max_speed_x:
		velocity.x = velocity.x / abs(velocity.x) * max_speed_x
	
	move_and_slide(velocity, Vector2.UP)


var tile_to_mine : Vector2
func mine_in_direction(direction: Vector2):
	tile_to_mine = tilemap.world_to_map(global_position) + direction
	
	if tile_to_mine.x < 1 || tile_to_mine.y < 1:
		return # cant mine into negatives due to water system
	
	if !tilemap.is_tile_mineable(tile_to_mine):
		return # cant mine this tile
	
	if direction == Vector2.LEFT:
		$MiningItem.scale.x = 1
		$MiningItem.rotation_degrees = 0
	elif direction == Vector2.RIGHT:
		$MiningItem.scale.x = -1
		$MiningItem.rotation_degrees = 0
	elif direction == Vector2.DOWN:
		$MiningItem.scale.x = 1
		$MiningItem.rotation_degrees = -90
		
	$MiningAnimationPlayer.play("mine")
	
func complete_mine():
	if (tilemap.world_to_map(global_position) - tile_to_mine).length_squared() <= 1:
		tilemap.mine_tile(tile_to_mine)
		
func check_for_water_area():
	var check = $WaterAreaScanner.get_overlapping_areas().size() > 0
	if check && !is_in_water: # newly in water
		# cancel any pending fall damage since we hit water
		$FallTimer.stop()
		take_fall_damage = false
		
		if !AudioServer.is_bus_mute(0):
			$WaterSplashAudio.play()
		
	is_in_water = check
	
func check_for_underwater():
	var check = $UnderwaterScanner.get_overlapping_areas().size() > 0
	
	if check && !is_underwater:
		$AirTimer.start() # newly underwater
		$Bubbles.emitting = true
		$PlayerUI/AirBar.visible = true
		$UnderwaterPlayer.play("underwater")
		
		AudioServer.set_bus_volume_db(3, -10)
		if !AudioServer.is_bus_mute(0):
			$UnderwaterAudio.play()
	
	if !check && is_underwater:
		$AirTimer.stop() # newly out of water
		$AirTimer/AirDamageTimer.stop()
		$Bubbles.emitting = false
		$PlayerUI/AirBar.visible = false
		$UnderwaterPlayer.play("out of water")
		
		AudioServer.set_bus_volume_db(3, 0)
		$UnderwaterAudio.stop()
		
	
	is_underwater = check
	
func check_for_climbable():
	is_climbing = $ClimbableScanner.get_overlapping_areas().size() > 0
	
	
		
func take_damage(dmg):
	if is_dead:
		return
	health = max(health - dmg, 0)
	update_health_bar()
	
	if health == 0:
		die()
	else:
		$HurtPlayer.play("hurt")
	
func die():
	is_dead = true
	for child in $PlayerUI.get_children():
		child.visible = false
	$ItemContainer/CanvasLayer/ActiveItemBox.visible = false
	$Bubbles.emitting = false
	$BodyAnimationPlayer.play("death")
	emit_signal("died", score)

func add_score(amt):
	
	# check if difficulty needs increased
	var previous = round(score/200)
	var new = round((score+amt)/200)
	if new > previous:
		for i in range(new-previous):
			tilemap.get_node("WaterFUNTimer").wait_time = tilemap.get_node("WaterFUNTimer").wait_time * 0.75
	
	tilemap.get_node("WaterFUNTimer").wait_time = max(tilemap.get_node("WaterFUNTimer").wait_time, 2.5)
	score += amt
	$PlayerUI/ScoreEnbiggener/ScoreLabel.text = str(score) + " points"
	
func update_health_bar():
	var heart_halves = health / 10
	
	for i in range(1,6):
		get_node("PlayerUI/HealthSection/GridContainer/HeartContainer"+str(i)+"/Health-heart").frame = 0 if heart_halves >= 2 else 2 - max(heart_halves, 0)
		heart_halves -= 2
	
	

func _on_AirDamageTimer_timeout():
	take_damage(10)


func _on_AirTimer_timeout():
	$AirTimer/AirDamageTimer.start()


func _on_PickupScoreScanner_area_entered(area):
	if area.owner.has_method("picked_up"):
		add_score(area.owner.points)
		area.owner.picked_up()


func _on_ClimbableScanner_area_entered(area):
	check_for_climbable()
func _on_ClimbableScanner_area_exited(area):
	check_for_climbable()

var take_fall_damage := false
func _on_FallTimer_timeout():
	take_fall_damage = true
