extends Node2D


onready var leaderboard_scene = preload("res://leaderboard/Leaderboard.tscn")


func _enter_tree():
	if not Seed.world_seed:
		randomize()
		Seed.world_seed = str(randi())
		Seed.world_name = CaveNamer.cave_name()
	rand_seed(Seed.world_seed.hash())
	Seed.world_name = CaveNamer.cave_name()
	$WorldTileMap/CaveGenerator.world_seed = Seed.world_seed
	print("Seed is ", Seed.world_seed, Seed.world_name)
	print("hash is ", Seed.world_seed.hash())

# Called when the node enters the scene tree for the first time.
func _ready():
	$LeaderboardLayer/EndGameStuff/EndGameAdjustmentNode2D/CaveName.text = Seed.world_name
	$LeaderboardLayer/Node2D/IntroText.text = "Now entering \n\n" + Seed.world_name
	

func _process(delta):
	if Input.is_action_just_pressed("mute"):
		AudioServer.set_bus_mute(0, !AudioServer.is_bus_mute(0))

var player_score 
func _on_Player_died(score):
	player_score = score
	$LeaderboardLayer/EndGameStuff/EndGameAdjustmentNode2D/FinalScore.text = "FINAL SCORE: " + str(score) + " points"
	$LeaderboardLayer/EndGamePlayer.play("death")


func _on_Submit_Score_pressed():
	var lb = leaderboard_scene.instance()
	lb.show_submit_score = player_score > 0
	lb.submit_score_value = player_score
	lb.show_new_game_button = true
	lb.submit_score_meta = {
		"world_name": Seed.world_name,
		"world_seed": Seed.world_seed
	}
	$LeaderboardLayer.add_child(lb)
	$LeaderboardLayer/EndGameStuff.visible = false


func _on_View_Leaderboard_pressed():
	# Doing this because people will be sad to lose a score
	_on_Submit_Score_pressed()
	return
	
	var lb = leaderboard_scene.instance()
	lb.show_submit_score = player_score > 0
	lb.show_new_game_button = true
	$LeaderboardLayer.add_child(lb)
	$LeaderboardLayer/EndGameStuff.visible = false


func _on_PlayAgain_pressed():
	get_tree().change_scene("res://Game.tscn")
