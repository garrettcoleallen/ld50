extends TileMap
class_name WorldTileMap

export (NodePath) var WaterSystemPath

onready var water_system = get_node(WaterSystemPath)

var water_spawn_scene = preload("res://WaterSpawn.tscn")
var coin_scene = preload("res://score-items/Coin.tscn")
var egg_chamber_scene = preload("res://score-items/EggChamber.tscn")
var egg_hint_scene = preload("res://score-items/EggHint.tscn")
var camp_scene = preload("res://score-items/Camp.tscn")

var chest_scene = preload("res://score-items/Chest.tscn")
var nf_scene = preload("res://score-items/NightmareFace.tscn")
var minecart_scene = preload("res://score-items/Minecart.tscn")
var skeleton_scene = preload("res://score-items/SkeleZone.tscn")


var light_treasures_per_map := 25


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


func mine_tile(tile_pos: Vector2, mute_sound: bool = false) :
	var current = get_cellv(tile_pos)
	if current >= 0 and current <= 2:
		set_cellv(tile_pos, current+1)
	elif current == 3:
		set_cellv(tile_pos, -1)
		$MinedTilePlayer.global_position = map_to_world(tile_pos)
		$MinedTilePlayer.play()
		
func is_tile_mineable(tile_pos:Vector2) -> bool:
	var current = get_cellv(tile_pos)
	return current >= 0 and current <= 3


## PROC GEN WISH ME LUCK WOOO
func create_water_spawn_near_player():
	
	
	var p_pos = world_to_map($Player.global_position)
	var possible_spawns := []
	
	for x in range(-15,15):
		if abs(x) < 3:
			continue #skip anything <5 away
		for y in range(-15,15):
			if abs(y) < 3:
				continue
			
			var check = p_pos + Vector2(x,y)
			if _tile_is_ceiling(check) && is_tile_mineable(check):
				possible_spawns.append(check)
		
	
	if possible_spawns.size() > 0:
		possible_spawns.shuffle()
		var found_spawn_pos = possible_spawns.pop_front()
		
		var water_spawn = water_spawn_scene.instance() as WaterSpawn
		water_spawn.water_system = water_system
		water_spawn.water_spawn_amount = 8
		water_spawn.global_position = map_to_world(found_spawn_pos)
		add_child(water_spawn)
		set_cellv(found_spawn_pos, -1) #clear the ceiling and make it water
	
	# spawn a second smaller random one if possible 50% of the time
	if possible_spawns.size() > 0 && randf() > 0.5:
		var found_spawn_pos = possible_spawns.pop_front()
		
		var water_spawn = water_spawn_scene.instance() as WaterSpawn
		water_spawn.water_system = water_system
		water_spawn.water_spawn_amount = rand_range(4,8)
		water_spawn.water_spawn_lifetime = rand_range(1,7)
		water_spawn.water_spawn_frequency = rand_range(0.001, 1)
		water_spawn.global_position = map_to_world(found_spawn_pos)
		add_child(water_spawn)
		set_cellv(found_spawn_pos, -1) #clear the ceiling and make it water	
	
	
func _tile_is_ceiling(pos):
	return get_cellv(pos) >= 0 && get_cellv(pos+Vector2(0,1)) == -1
		
	


func _on_CaveGenerator_map_generation_complete():
		
	_spawn_extra_prefabs()
	
	_spawn_tunnels()
	
	_spawn_light_treasures()
	
	spawn_egg_treasure()
	
	# always last to ensure safety
	_player_spawn_area_cleanup()
	
	# has to come after player_spawn for hint direction
	spawn_egg_hints()
	
	_bound_the_world()
	
	
func _spawn_extra_prefabs():
	
	var map_size = get_used_rect()
	var padding := 10
	for i in range(15):
		_spawn_skelezone(Vector2(padding + (randi() % int(map_size.end.x-padding)), padding + (randi() % int(map_size.end.y-padding))))
		
	for i in range(10):
		_spawn_chest(Vector2(padding + (randi() % int(map_size.end.x-padding)), padding + (randi() % int(map_size.end.y-padding))))
		
	for i in range(7):
		_spawn_minecart(Vector2(padding + (randi() % int(map_size.end.x-padding)), padding + (randi() % int(map_size.end.y-padding))))
		
	for i in range(1):
		_spawn_nf(Vector2(padding + (randi() % int(map_size.end.x-padding)), padding + (randi() % int(map_size.end.y-padding))))

#TODO - redo this whole thing because its not good
func spawn_egg_hints():
	
	var player_pos = $Player.global_position
	var egg_pos = egg_chamber_pos
	
	var hints_to_spawn = 3
	
	var player_direction : Vector2 = player_pos - egg_pos
	
	var hint_distance : Vector2 = player_direction/hints_to_spawn/2
	
	var intensity_adjustment = 10/hints_to_spawn
	
	var hint_loc_variance := 64
	
	for i in range(hints_to_spawn):
		var hint = egg_hint_scene.instance()
		hint.intensity = 10 - (intensity_adjustment * i)
		hint.global_position = (egg_pos + (hint_distance * (i+1))) + Vector2(rand_range(-hint_loc_variance, hint_loc_variance), rand_range(-hint_loc_variance, hint_loc_variance))
		call_deferred("add_child", hint)


func _spawn_light_treasures():
	var map_size = get_used_rect()
	for i in range(0,light_treasures_per_map):
		var coin = coin_scene.instance()
		var havent_placed = true
		
		while havent_placed:
			var x = randi() % int(map_size.end.x)
			var y = randi() % int(map_size.end.y)
			if get_cell(x,y) == -1:
				havent_placed = false
				coin.global_position = map_to_world(Vector2(x,y))
				call_deferred("add_child", coin)


var egg_chamber_pos # Egg chamber scene for building around
func spawn_egg_treasure():
	var map_size = get_used_rect()
	var padding := 10
	
	var spawn = Vector2(padding + (randi() % int(map_size.end.x-padding)), padding + (randi() % int(map_size.end.y-padding)))
	
	var egg = egg_chamber_scene.instance()
	
	egg_chamber_pos = spawn
	egg.global_position = map_to_world(spawn)
	call_deferred("add_child", egg)
	
	var hard_walls = []
	var chamber_spawn = spawn
	var box_size = 10
	for x in range(-box_size,box_size+1):
		for y in range(-box_size,1):
			# Set hard walls left and right
			if abs(x) == box_size:
				set_cell(x+chamber_spawn.x, y+chamber_spawn.y, 4)
				hard_walls.append(Vector2(x+chamber_spawn.x, y+chamber_spawn.y))
			else:
				set_cell(x+chamber_spawn.x, y+chamber_spawn.y, -1)
			
		# set hard walls top and bottom
		set_cell(x+chamber_spawn.x, chamber_spawn.y-box_size, 4)
		set_cell(x+chamber_spawn.x, chamber_spawn.y, 4)
		
	# Put holes in chamber to get in
	hard_walls.shuffle()
	for i in range(3):
		set_cellv(hard_walls.pop_front(), -1)
		
		
	

func _bound_the_world():
	var map_size = get_used_rect()
	for x in range(0, map_size.end.x):
		set_cell(x,0, 4)
		set_cell(x,map_size.end.y, 4)
	for y in range(0, map_size.end.y):
		set_cell(0, y, 4)
		set_cell(map_size.end.x, y, 4)

	
func _player_spawn_area_cleanup():
	$Player.global_position = map_to_world(get_used_rect().end/2)
	var p_spawn = world_to_map($Player.global_position)
	var box_size = 3
	for x in range(-box_size,box_size):
		for y in range(-box_size,1):
			set_cell(x+p_spawn.x, y+p_spawn.y, -1)
		set_cell(x+p_spawn.x, 2+p_spawn.y, 0)
			
	for x in range(-box_size, box_size):
		set_cell(x+p_spawn.x, p_spawn.y-box_size, 4)
		set_cell(x+p_spawn.x, p_spawn.y+1, 4)
		
	# Add a light coin next to player to give idea
	var coin = coin_scene.instance()
	coin.global_position = $Player.global_position + Vector2(30,0)
	call_deferred("add_child", coin)

func _spawn_tunnels():
	var tunnels = 5
	var tunnel_length = [10,25]
	var padding = 20
	var map_size = get_used_rect()
	for i in range(tunnels):
		var spawn_loc = Vector2(padding + (randi() % int(map_size.end.x-padding)), padding + (randi() % int(map_size.end.y-padding)))
		var distance = round(rand_range(tunnel_length[0], tunnel_length[1]))
		#_spawn_tunnel(spawn_loc, distance, i % 2 == 0)
		_spawn_tunnel(spawn_loc, distance, true)


func _spawn_tunnel(start, distance, spawn_camp = false):
	var direction = rand_range(0,360)
	var direction_v = Vector2(cos(direction), sin(direction))
	
	for i in range(distance):
		set_cellv((direction_v * i) + start, -1)
		set_cellv((direction_v * i) + start + Vector2.UP, -1)
		set_cellv((direction_v * i) + start + Vector2.UP + Vector2.LEFT, -1)
		set_cellv((direction_v * i) + start + Vector2.UP + Vector2.RIGHT, -1)
		set_cellv((direction_v * i) + start + Vector2.LEFT, -1)
		set_cellv((direction_v * i) + start + Vector2.RIGHT, -1)
	
	if spawn_camp:
		var camp = camp_scene.instance()
		camp.global_position = map_to_world(start)
		call_deferred("add_child", camp)
		
		var box_size = 3
		for x in range(-box_size,box_size+1):
			for y in range(-box_size,1):
				set_cell(x+start.x, y+start.y, -1)

			# set bottom
			set_cell(x+start.x, start.y, 0)
		
func _spawn_chest(map_pos):
	var instance = chest_scene.instance()
	instance.global_position = map_to_world(map_pos)
	call_deferred("add_child", instance)
	
	var box_size = 3
	for x in range(-box_size,box_size+1):
		for y in range(-box_size,1):
			set_cell(x+map_pos.x, y+map_pos.y, -1)

			# set bottom
			set_cell(x+map_pos.x, map_pos.y, 0)
		for y in range(0, box_size*2):
			set_cell(x+map_pos.x, y+map_pos.y, 0)
			
func _spawn_nf(map_pos):
	var instance = nf_scene.instance()
	instance.global_position = map_to_world(map_pos)
	call_deferred("add_child", instance)
	
	var box_size = 1
	for x in range(-box_size,box_size+1):
		for y in range(-box_size,1):
			set_cell(x+map_pos.x, y+map_pos.y, -1)

			# set bottom
			set_cell(x+map_pos.x, map_pos.y, 0)
			
			
func _spawn_minecart(map_pos):
	var instance = minecart_scene.instance()
	instance.global_position = map_to_world(map_pos)
	call_deferred("add_child", instance)
	
	var box_size = 4
	for x in range(-box_size,box_size+1):
		for y in range(-box_size,1):
			set_cell(x+map_pos.x, y+map_pos.y, -1)

			# set bottom
			set_cell(x+map_pos.x, map_pos.y, 0)
			
func _spawn_skelezone(map_pos):
	var instance = skeleton_scene.instance()
	instance.global_position = map_to_world(map_pos)
	call_deferred("add_child", instance)
	
	var box_size = 3
	for x in range(-box_size,box_size+1):
		for y in range(-box_size,1):
			set_cell(x+map_pos.x, y+map_pos.y, -1)

			# set bottom
			set_cell(x+map_pos.x, map_pos.y, 0)



func _on_WaterFUNTimer_timeout():
	create_water_spawn_near_player()

var water_level
func _on_RisingWaterTimer_timeout():
	return # Ran out of time, moving ot intro and cave name instead
#	var map_size = get_used_rect()
#	if not water_level:
#		water_level =  map_size.end.y
#
#	for i in range()
	
