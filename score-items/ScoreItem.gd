extends Node2D
class_name ScoreItem

export (int) var points = 10

signal picked_up

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

var already_scored := false
func picked_up():
	if already_scored:
		return
	already_scored = true
	
	$AudioStreamPlayer2D.play()
	
	$PickupArea2D/CollisionShape2D.set_deferred("disabled", true)
	
	$Tween.interpolate_property(get_parent(), "global_position",
		get_parent().global_position, get_parent().global_position + Vector2(0,-50), 0.5,
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		
	$Tween.interpolate_property(get_parent(), "modulate",
		Color(1,1,1,1), Color(1,1,1,0), 0.5,
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	
	$Tween.start()
	
	emit_signal("picked_up")
	


func _on_Tween_tween_completed(object, key):
	get_parent().queue_free()
