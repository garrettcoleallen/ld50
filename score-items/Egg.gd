extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_ScoreItem_picked_up():
	var tilemap = get_tree().get_nodes_in_group("world_tilemap").front()
	tilemap.spawn_egg_treasure()
	tilemap.spawn_egg_hints()
	for i in range(5):
		tilemap.create_water_spawn_near_player()
