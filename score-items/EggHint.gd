extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (int, 0, 10) var intensity = 5


# Called when the node enters the scene tree for the first time.
func _ready():
	modulate.a = (intensity/10.0)
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
