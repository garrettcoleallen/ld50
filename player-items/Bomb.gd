extends Node2D

var active_bomb_scene = preload("res://player-items/ActiveBomb.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func use():
	if $CooldownTimer.is_stopped():
		print("use bomb!")
		$CooldownTimer.start()
		_throw_bomb()
	else:
		print("Cooldown not over")

func _on_CooldownTimer_timeout():
	pass # Replace with function body.


func _throw_bomb():
	var bomb = active_bomb_scene.instance()
	bomb.global_position = global_position
	get_tree().root.add_child(bomb)

func get_item_sprite() -> Sprite:
	var sprite = $Bomb.duplicate()
	return sprite
	
func get_cooldown_percent() -> float:
	if $CooldownTimer.is_stopped():
		return 0.0
	return $CooldownTimer.time_left/$CooldownTimer.wait_time
