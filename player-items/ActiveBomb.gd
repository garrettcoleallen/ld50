extends RigidBody2D


const DESTROY_DISTANCE = 2
const MED_DMG_DISTANCE = 3
const SML_DMG_DISTANCE = 4


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_FuseTimer_timeout():
	
	var tilemap = get_tree().get_nodes_in_group("world_tilemap").front() as WorldTileMap
	var tilemap_pos = tilemap.world_to_map(global_position)
	
	#check player for damage
	var player = get_tree().get_nodes_in_group("player").front()
	var player_tilemap_pos = tilemap.world_to_map(player.global_position)
	var player_distance = abs((player_tilemap_pos - tilemap_pos).length())
	if player_distance <= SML_DMG_DISTANCE:
		player.take_damage(20)
	if player_distance <= MED_DMG_DISTANCE:
		player.take_damage(20)
	if player_distance <= DESTROY_DISTANCE:
		player.take_damage(100)
	
	# destroy tiles
	for x in range(-DESTROY_DISTANCE, DESTROY_DISTANCE+1):
		for y in range(-DESTROY_DISTANCE, DESTROY_DISTANCE+1):
			tilemap.water_system.clear_water(tilemap_pos + Vector2(x,y))
			tilemap.mine_tile(tilemap_pos + Vector2(x,y), true)
			tilemap.mine_tile(tilemap_pos + Vector2(x,y), true)
			
	for x in range(-MED_DMG_DISTANCE, MED_DMG_DISTANCE+1):
		for y in range(-MED_DMG_DISTANCE, MED_DMG_DISTANCE+1):
			tilemap.water_system.clear_water(tilemap_pos + Vector2(x,y))
			tilemap.mine_tile(tilemap_pos + Vector2(x,y), true)
			
	for x in range(-SML_DMG_DISTANCE, SML_DMG_DISTANCE+1):
		for y in range(-SML_DMG_DISTANCE, SML_DMG_DISTANCE+1):
			tilemap.water_system.clear_water(tilemap_pos + Vector2(x,y))
			tilemap.mine_tile(tilemap_pos + Vector2(x,y), true)
			
	$AnimationPlayer.play("explode")
	
	
