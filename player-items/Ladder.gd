extends Node2D

var active_ladder_scene = preload("res://player-items/ActiveLadder.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func use():
	if _can_use_ladder():
		
		_use_ladder()
		$CooldownTimer.start()
	else:
		print("Cooldown not over")

func _on_CooldownTimer_timeout():
	pass # Replace with function body.


func _can_use_ladder():
	var player = get_tree().get_nodes_in_group("player").front()
	if player.is_on_floor() || player.is_on_wall() || player.is_on_ceiling() || player.is_climbing:
		return $CooldownTimer.is_stopped()
		
	return false
	

func _use_ladder():
	var tilemap = get_tree().get_nodes_in_group("world_tilemap").front()
	for i in range(0,5):
		var ladder = active_ladder_scene.instance()
		ladder.global_position = global_position - (Vector2(0,32) * i)
		tilemap.add_child(ladder)
		$AudioStreamPlayer2D.play()
		
	pass
#	var bomb = active_ladder_scene.instance()
#	bomb.global_position = global_position
#	get_tree().root.add_child(bomb)

func get_item_sprite() -> Sprite:
	var sprite = $Sprite.duplicate()
	return sprite
	
func get_cooldown_percent() -> float:
	if $CooldownTimer.is_stopped():
		return 0.0
	return $CooldownTimer.time_left/$CooldownTimer.wait_time
